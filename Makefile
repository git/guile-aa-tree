## Makefile - Makefile for building the Guile AA Tree package
## Copyright (C) 2017  Christopher Howard

## Guile AA Tree is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## Guile AA Tree is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

SITEDIR := $(shell guile -c '(simple-format \#t (%site-dir))')
CCACHEDIR := $(shell guile -c '(simple-format \#t (%site-ccache-dir))')

all : aa-tree.go
.PHONY : all

aa-tree.go :
	guild compile -o aa-tree.go aa-tree.scm

install : aa-tree.go
	install -t $(SITEDIR) aa-tree.scm
	install -d $(CCACHEDIR)
	install -t $(CCACHEDIR) aa-tree.go

uninstall:
	rm -f $(SITEDIR)/aa-tree.scm $(CCACHEDIR)/aa-tree.go

clean:
	rm -f aa-tree.go
	rm -f test-aa-tree.log

tests:
	guile test-aa-tree.scm
