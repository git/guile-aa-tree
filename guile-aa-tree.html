<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>guile-aa-tree &mdash; an AA tree implementation for Guile
      scheme</title>
  </head>
  <body lang="en-US" dir="ltr">
    <h1>guile-aa-tree &mdash; an AA tree implementation for Guile scheme</h1>
    <p>This module provides non-mutating insert and delete operations as
      well as common operations such as walking the tree. It also
      features "nested" versions of insert, delete, and search
      functions, that allow operations on nested trees.</p>
    <h2>Copying this documentation</h2>
    <p>guile-aa-tree.html - documentation file for Guile AA Tree</p>
    <p>Copyright (C) 2017  Christopher Howard</p>
    <p>Guile AA Tree is free software: you can redistribute it and/or
      modify it under the terms of the GNU General Public License as
      published by the Free Software Foundation, either version 3 of the
      License, or (at your option) any later version.</p>
    <p>Guile AA Tree is distributed in the hope that it will be useful, but
      WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
      General Public License for more details.</p>
    <p>You should have received a copy of the GNU General Public License
      along with this program.  If not,
      see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
    <h2>Using the module</h2>
    <pre>(use-modules (aa-tree))</pre>
    <h2>Empty trees</h2>
    <p>The empty tree is represented by a #nil value. For example, to
      create a tree with one node, you could call this expression:</p>
    <pre>(aa-insert #nil 'animals 'moose)</pre>
    <h2>Keys</h2>
    <p>In the <i>unextended</i> operation of the module, it is
      necessary that every key in a tree must be either a number,
      string, identifier, or keyword. Furthermore, every key of every node in
      a tree must be the same type.</p>
    <p>If you wish to use other key types or have multiple keys types
      in a tree, it is possible to do this by defining an
      appropriate <a href="https://www.gnu.org/software/guile/manual/html_node/GOOPS.html">goops</a>
      comparison methods for the type(s) you wish to use. More
      specifically, you must define an appropriate <i>equal?</i>
      method and <i>&lt;</i> method.</p>
    <p>Some functions will also take a list of multiple keys instead
      of a single key, which signals that a nested operation is
      intended.</p>
    <h2>Nested operations</h2>
    <p>A "nested" tree refers to a tree which is the value of a node
      in another tree. For convenience, <i>aa-delete, aa-insert,</i>
      and <i>aa-search</i> can operate on a set of nested trees, by
      receiving a list of keys instead of a single
      key. <i>aa-insert</i> can even create nested trees in this
      way. For example, in a game you might want to store data like
      so:</p>
    <pre>(aa-insert world-tree (list 'rooms 'kitchen 'object-descriptions 'knife) "A sharp cutting tool.")</pre>
    <h2>Procedures</h2>
    <p>Scheme procedure:
      <b>aa-delete</b> <i>tree key</i></p>
    <p>
      Performs a non-mutating delete on <i>tree</i>. Deletes the node in
      <i>tree</i> which has key <i>key.</i> If there is no such node, it
      simply returns <i>tree.</i> O(log n) worst case efficiency.</p>
    <p>
      If a list of keys is provided instead of a single key, <i>aa-delete
      </i>will attempt a nested tree delete, starting the search with the
      first key in the list. If a nested tree of some required level does
      not exist, it will simply return the original tree unmodified.</p>
    <p>Scheme procedure:
      <b>aa-for-each</b> <i>proc tree</i></p>
    <p>Calls <i>proc</i> for each of the key-value pairs
    in <i>tree</i>. <i>proc</i> must accept an argument of the form
    (key . value). The calls will be made in order of least to
    greatest keys. <i>aa-for-each</i> does not return the results of
    these calls.</p>
    <p>Scheme procedure:
      <b>aa-for-each-reverse</b> <i>proc tree</i></p>
      <p><i>aa-for-each-reverse</i> operates the same
      as <i>aa-for-each</i> except that it applies the pairs in reverse
      order, from greatest key to least key.</p>
    </p>
    <p>Scheme procedure:
      <b>aa-insert</b> <i>tree key </i>#:optional
      <i>value</i>)</p>
    <p>Performs a non-mutating insert on <i>tree,</i> inserting a node
      having key <i>key</i> and value <i>value.</i>  O(log n) worst
      case efficiency.</p>
    <p>If a list of keys is provided instead of a single
      key, <i>aa-insert</i> will attempt a nested tree insert,
      starting the search with the first key in the list. If a nested
      tree of some required level does not exist, that tree will be
      created and inserted into the the previous level tree.</p>
    <p>Scheme procedure:
      <b>aa-map</b> <i>fn tree</i></p>
    <p>Maps <i>fn</i> over the key-value pairs in <i>tree</i>, ordered
      from smallest to greatest key, returning the list of
      results. So, <i>fn</i> must take an argument of the form <i>(key
      . value)</i>. Unlike <i>aa-for-each</i>, the actual functional
      calls will not necessarily be executed in the same order as the
      key-value pairs. O(n) efficiency.</p>
    <p>Scheme procedure:
      <b>aa-map-keys</b> <i>fn tree</i></p>
    <p>The same operation as <i>aa-map</i>, except only the key of
      each node is presented to <i>fn</i>. O(n) efficiency.</p>
    <p>Scheme procedure:
      <b>aa-map-reverse</b> <i>fn tree</i></p>
    <p>The equivalent of <i>aa-map</i>, but the ordering is from
      greatest to smallest key. O(n) efficiency.</p>
    <p>Scheme procedure:
      <b>aa-map-values</b> <i>fn tree</i></p>
    <p>The same operation as <i>aa-map</i>, except only the value of
      each node is presented to <i>fn</i>. O(n) efficiency.</p>
    <p>Scheme procedure:
      <b>aa-modify</b> <i>tree key fn</i></p>
    <p>A functional setter for node values. Uses aa-search to search
      for a node in TREE, then feeds the value of that node to FN, and
      then uses aa-insert to insert the node back into TREE, but
      containing the new value returned from FN. This is an immutable
      operation provided of course you don't mutate the object passed
      to FN. This provides a convenient way to modify a value in the
      tree, without actually mutating a real node in memory.</p>
    <p>Scheme procedure:
      <b>aa-search</b> <i>tree key</i></p>
    <p>Searches for the node in the tree with key <i>key,</i> and
      returns the value of that node. If no such key is found, a
      key-not-found exception will be thrown, with the key as the
      first argument. O(log n) worst case efficiency.</p>
    <p>If a list of keys is provided instead of a single
      key, <i>aa-search</i> will attempt a nested tree search,
      starting the search with the first key in the list. If a nested
      tree of some required level does not exist, the function will
      throw an exception.</p>
    <p>Scheme procedure:
      <b>aa-search-wrapped</b> <i>tree key</i></p>
    <p>Provides the same functionality as <i>aa-search</i>, except
    that (1) return values are wrapped in a single element list; (2)
    if the key is not found, <i>aa-search-wrapped</i> returns
    #nil. This approach allows you to avoid the possibility of
    throwing an exception, while still allowing you to distinquish
    between not finding a node, and finding a node which stores #nil as
      its value.</p>
  <p>While this approach does avoid the use of exceptions, note that
  it is easy to forget to unwrap (i.e., car) a result before using
  it.</p>
  </p>
    </p>
    <p>Scheme procedure:
      <b>aa-to-list</b> <i>tree</i></p>
    <p>Returns the key value pairs of <i>tree</i> in list form,
      ordering from smallest to greatest key. O(n) efficiency.</p>
    <p>Scheme procedure:
      <b>list-to-aa-tree</b> <i>init-tree</i> <i>lst</i></p>
    <p>Takes keys and values from <i>lst</i> and inserts them into
      tree <i>init-tree</i>. If <i>init-tree</i> is #nil (empty tree)
      then this is the same as converting <i>lst</i> into a
      tree. Argument <i>lst</i> must be a list of the form (KEY VALUE
      KEY VALUE ...) with zero or more sets of keys and values.</p>
    <p>KEYs may be any type accepted by <i>aa-insert</i>. Note that
      using another list in place of a single key will still signal a
      nested insert operation.</p>
    <p>If a VALUE is itself a list, then VALUE will be processed
      by <i>list-to-aa-tree</i>, creating another tree stored as the
      value of that node (a nested tree). If you wish simply to use a
      list as a node value, then use the identifer <i>_</i> as the
      first element of the list. That first <i>_</i> will be removed
      before the node is added.</p>
    <p>Example:</p>
    <pre>
(read-set! keywords 'prefix)

(define game-data
   (list-to-aa-tree #nil
      '(:troops
          (:infantry
             (:speed medium :armor weak)
           :heavy-infantry
             (:speed slow :armor strong))
        :city-names
          (_ Aberdeen Brighton Cambridge Exeter Glasgow))))</pre>
    <p>It is not necessary to use the keyword type for keys, but it
      does make the data structure more readable.</p>
</body>
</html>
