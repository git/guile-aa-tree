;; test-aa-tree.scm - unit tests for Guile AA Tree module
;; Copyright (C) 2017  Christopher Howard

;; Guile AA Tree is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Guile AA Tree is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-aa-tree)
  #:use-module (aa-tree)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64))

(test-begin "test-aa-tree")

(test-equal
    (let ((tree
           (fold (lambda (kv t) (aa-insert t (car kv) (cdr kv))) #nil
                 '(("e" . 5)
                   ("b" . 2)
                   ("q" . 17)
                   ("h" . 8)
                   ("m" . 14)
                   ("e" . update)
                   ("f" . 6)))))
      (fold (lambda (k l) (cons (aa-search-wrapped tree k) l)) '()
            (list "m" "h" "f" "e" "b" "q" "t")))
  '(#nil (17) (2) (update) (6) (8) (14)))

(test-equal
    (let ((tree
           (fold (lambda (kv t) (aa-insert t (car kv) (cdr kv))) #nil
                 '(("e" . 5)
                   ("b" . 2)
                   ("q" . 17)
                   ("h" . 8)
                   ("m" . 14)
                   ("e" . update)
                   ("f" . 6)))))
      (fold (lambda (k l) (cons (aa-search tree k) l)) '()
            (list "m" "h" "f" "e" "b" "q")))
  '(17 2 update 6 8 14))

(test-equal
    (let ((tree
           (fold (lambda (kv t) (aa-insert t (car kv) (cdr kv))) #nil
                 '(("e" . 5)
                   ("b" . 2)
                   ("q" . 17)
                   ("h" . 8)
                   ("m" . 14)
                   ("e" . update)
                   ("f" . 6)))))
      (catch 'key-not-found
        (lambda () (aa-search tree "t"))
        (lambda (k e) (list k e))))
  '(key-not-found "t"))

(test-equal
    (let* ((tree1
            (fold (lambda (kv t) (aa-insert t (car kv) (cdr kv))) #nil
                  '((4 . "four")
                    (8 . "eight")
                    (2 . "two")
                    (3 . "three"))))
           (tree2
            (fold (lambda (k t) (aa-delete t k)) tree1
                  '(8 2))))
      (fold (lambda (k l) (cons (aa-search-wrapped tree2 k) l)) '()
            (list 3 2 4 8)))
  '(#nil ("four") #nil ("three")))

(test-equal
    (call-with-output-string
      (lambda (p)
        (aa-for-each
         (lambda (kv) (display (car kv) p))
         (aa-insert (aa-insert (aa-insert #nil "b" 2) "e" 5) "a" 1))))
  "abe")

(test-equal
    (call-with-output-string
      (lambda (p)
        (aa-for-each-reverse
         (lambda (kv) (display (car kv) p))
         (aa-insert (aa-insert (aa-insert #nil "b" 2) "e" 5) "a" 1))))
  "eba")

(test-equal
    (aa-map-keys
     (lambda (k) (string-append k "q"))
     (aa-insert (aa-insert (aa-insert #nil "b" 2) "e" 5) "a" 1))
  '("aq" "bq" "eq"))

(test-equal
    (aa-map-reverse
     (lambda (kv) (string-append (car kv) "q"))
     (aa-insert (aa-insert (aa-insert #nil "b" 2) "e" 5) "a" 1))
  '("eq" "bq" "aq"))

(test-equal
    (aa-map-values
     (lambda (v) (* 2 v))
     (aa-insert (aa-insert (aa-insert #nil "b" 2) "e" 5) "a" 1))
  '(2 4 10))

(test-equal
    (aa-to-list (aa-insert (aa-insert (aa-insert #nil "e" 5) "a" 1) "b" 2))
  '(("a" . 1) ("b" . 2) ("e" . 5)))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one") 5 "five"))
           (tree2 (aa-insert tree1 (list 4) "four")))
      (aa-search-wrapped tree2 4))
  (list "four"))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one") 5 "five"))
           (tree2 (aa-insert tree1 4
                             (aa-insert
                              (aa-insert
                               (aa-insert #nil "b" 2) "f" 6) "c" 3)))
           (tree3 (aa-insert tree2 (list 4 "e") 5)))
      (aa-search-wrapped (car (aa-search-wrapped tree3 4)) "e"))
  (list 5))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one")
             5
             (aa-insert
              (aa-insert
               (aa-insert #nil "c" 3) "a" 1)
              "b"
              (aa-insert
               (aa-insert
                (aa-insert #nil 'b 2) 'e 5) 'd 4))))
           (tree2
            (aa-insert tree1 (list 5 "b" 'h) 8)))
      (aa-search-wrapped (car (aa-search-wrapped (car (aa-search-wrapped tree2 5)) "b")) 'h))
  '(8))

(test-assert
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one") 5 "five"))
           (tree2 (aa-delete tree1 (list 1))))
      (and
       (equal? (aa-search-wrapped tree2 1) #nil)
       (equal? (aa-search-wrapped tree2 5) '("five"))
       (equal? (aa-search-wrapped tree2 3) '("three")))))

(test-assert
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one") 5 "five"))
           (tree2 (aa-insert tree1 4
                             (aa-insert
                              (aa-insert
                               (aa-insert #nil "b" 2) "f" 6) "c" 3)))
           (tree3 (aa-delete tree2 (list 4 "f"))))
      (and
       (equal? (aa-search-wrapped (car (aa-search-wrapped tree3 4)) "b") '(2))
       (equal? (aa-search-wrapped (car (aa-search-wrapped tree3 4)) "f") #nil)
       (equal? (aa-search-wrapped (car (aa-search-wrapped tree3 4)) "c") '(3)))))

(test-assert
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one")
             5
             (aa-insert
              (aa-insert
               (aa-insert #nil "c" 3) "a" 1)
              "b"
              (aa-insert
               (aa-insert
                (aa-insert #nil 'b 2) 'e 5) 'd 4))))
           (tree2
            (aa-delete tree1 (list 5 "b" 'b))))
      (and
       (equal?
        (aa-search (aa-search (aa-search tree2 5) "b") 'd) 4)
       (equal?
        (aa-search-wrapped (car (aa-search-wrapped (car (aa-search-wrapped tree2 5)) "b")) 'd) '(4))
       (equal?
        (aa-search tree2 '(5 "b" d)) 4)
       (equal?
        (aa-search-wrapped tree2 '(5 "b" d)) '(4))
       (equal?
        (aa-search-wrapped (car (aa-search-wrapped (car (aa-search-wrapped tree2 5)) "b")) 'b) #nil)
       (equal?
        (catch 'key-not-found
          (lambda () (aa-search (aa-search (aa-search tree2 5) "b") 'b))
          (lambda (k a) (list k a)))
        '(key-not-found b))
       (equal?
        (catch 'key-not-found
          (lambda () (aa-search tree2 '(5 "b" b)))
          (lambda (k a) (list k a)))
        '(key-not-found b))
       (equal?
        (aa-search-wrapped (car (aa-search-wrapped (car (aa-search-wrapped tree2 5)) "b")) 'e) '(5)))))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one")
             5
             (aa-insert
              (aa-insert
               (aa-insert #nil "c" 3) "a" 1)
              "b"
              (aa-insert
               (aa-insert
                (aa-insert #nil 'b 2) 'e 5) 'd 4)))))
      (aa-search-wrapped tree1 (list 5 "b" 'e)))
  '(5))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one")
             5
             (aa-insert
              (aa-insert
               (aa-insert #nil #:c 3) #:a 1)
              #:b
              (aa-insert
               (aa-insert
                (aa-insert #nil 'b 2) 'e 5) 'd 4)))))
      (aa-search-wrapped tree1 (list 5 #:b 'e)))
  '(5))

(test-equal
    (let* ((tree1
            (aa-insert
             (aa-insert
              (aa-insert #nil 3 "three") 1 "one")
             5
             (aa-insert
              (aa-insert
               (aa-insert #nil "c" 3) "a" 1)
              "b"
              (aa-insert
               (aa-insert
                (aa-insert #nil 'b 2) 'e 5) 'd 4)))))
      (aa-search-wrapped tree1 (list 5 "q" 'e)))
  #nil)

(test-equal
    (let* ((tree1 (aa-insert #nil '(a e b) 19))
           (tree2 (aa-modify tree1
                             '(a e b)
                             (lambda (n) (* n 2)))))
      (aa-search tree2 '(a e b)))
  38)

(define-class <coin> ()
  (side #:init-value #t #:init-keyword #:side #:getter get-side))

(define-method (equal? (a <coin>) (b <coin>))
  (let ((x (get-side a))
        (y (get-side b)))
    (or (and x y)
        (and (not x)
             (not y)))))

(define-method (< (a <coin>) (b <coin>))
  (and (not (get-side a))
       (get-side b)))

(test-assert
  (let ((c1 (make <coin> #:side #t))
        (c2 (make <coin> #:side #f))
        (c3 (make <coin> #:side #t)))
    (let ((tree1
           (aa-insert
            (aa-insert
             (aa-insert #nil c1 "head1") c2 "tail1") c3 "head2")))
      (and (equal? (aa-search-wrapped tree1 c1) '("head2"))
           (equal? (aa-search-wrapped tree1 c2) '("tail1"))))))

(test-equal
    (let ((tree1
           (list-to-aa-tree #nil
            '(#:e 3
                  #:f
                  ("d" 5
                   "b" (_ foo bar))))))
      (aa-search-wrapped tree1 '(#:f "b")))
  '((foo bar)))

(test-end "test-aa-tree")
